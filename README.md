# whereabouts

A very simple API for getting the client's IP address, looks for X-Forwarded-For and falls back to RemoteAddr if not present. Obviously don't rely on this for anything serious since X-Forwarded-For can be spoofed.

## Usage

Download and chmod +x it.

Start listening
```
./whereabouts --port <PORT_NUMBER>
```

Requests
```
GET /
192.0.2.10

GET /json
{"ip":"192.0.2.10"}

GET /jsonp?callback=foo
foo({"ip":"192.0.2.10"});
```

## Release(s)

[whereabouts v1.0.0](https://gitlab.com/adilparvez/whereabouts/raw/master/release/1/0/0/whereabouts)

[whereabouts v1.1.0](https://gitlab.com/adilparvez/whereabouts/raw/master/release/1/1/0/whereabouts)

## License

The MIT License (MIT)
Copyright (c) 2016 Adil Parvez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

