package main

import (
    "gitlab.com/adilparvez/whereabouts/ip"
    "gopkg.in/alecthomas/kingpin.v2"
)

var port = kingpin.Flag("port", "Port number to listen on.").Short('p').Required().Int()

func main() {
    kingpin.Parse()

    ip.ListenAndServe(*port)
}

