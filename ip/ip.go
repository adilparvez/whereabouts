package ip

import (
    "encoding/json"
    "fmt"
    "net"
    "log"
    "net/http"
    "strconv"
    "strings"

    "github.com/gorilla/mux"
)

type IpAddr struct {
    Ip string `json:"ip"`
}

func getIp(r *http.Request) string {
    // Default to remote address.
    ip, _, _ := net.SplitHostPort(r.RemoteAddr)

    // Check X-Forwarded-For, and use that if it contains a valid ip. Don't
    // rely on this for anything serious, since it can be spoofed.
    chain := r.Header.Get("X-Forwarded-For")
    if len(chain) > 0 {
        temp := net.ParseIP(strings.Split(chain, ",")[0])
        if temp != nil {
            ip = temp.String()
        }
    }

    return ip
}

func getIpJson(r *http.Request) string {
    ip := getIp(r)
    j, _ := json.Marshal(IpAddr{ip})
    return string(j)
}

func plainHandler(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "text/plain")
    fmt.Fprintf(w, getIp(r))
}

func jsonHandler(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")

    ip := getIpJson(r)

    fmt.Fprintf(w, ip)
}

func jsonpHandler(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/javascript")

    callback := r.FormValue("callback")
    ip := getIpJson(r)

    fmt.Fprintf(w, callback + "(" + ip + ");")
}


func ListenAndServe(port int) {
    r := mux.NewRouter()

    r.HandleFunc("/", plainHandler).Methods("GET")
    r.HandleFunc("/json", jsonHandler).Methods("GET")
    r.HandleFunc("/jsonp", jsonpHandler).Methods("GET")

    addr := ":" + strconv.Itoa(port)
    log.Println("Listening on port", port)
    log.Fatal(http.ListenAndServe(addr, r))
}
